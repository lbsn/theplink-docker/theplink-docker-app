![version](https://lbsn.vgiscience.org/theplink-docker/theplink-docker-app/version.svg) ![pipeline](https://lbsn.vgiscience.org/theplink-docker/theplink-docker-app/pipeline.svg)

# theplink-docker.app

The app for theplink-docker is maintained in this separate repository and imported as a git submodule. This makes independent development possible, it can be tested and developed with proper base path (`/app`) and follows the separation of concerns principle. 