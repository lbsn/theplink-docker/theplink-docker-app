import requests

from app import crud
from app.core import config
from app.db.session import db_session
from app.lbsnmodels.post import PostCreate
from app.lbsnmodels.utils import uuid_to_guid
from app.tests.utils.utils import (
    random_lower_string, random_int, random_fullname, random_name_alias,
    random_sentence, random_uuid)


def create_random_post(
        origin_id: int = None, post_guid: str = None):
    user_id = 1
    user = crud.user.get(
        db_session=db_session, user_id=user_id)
    post_title = random_lower_string()
    post_body = random_sentence()
    post_guid = random_uuid()
    post_in = PostCreate(
        post_title=post_title,
        post_body=post_body)
    return crud.lbsnpost.create(
        db_session=db_session,
        post_in=post_in,
        user_guid=uuid_to_guid(user.uuid, user_id),
        post_guid=post_guid
    )
