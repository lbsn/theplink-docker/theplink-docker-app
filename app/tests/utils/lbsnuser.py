import requests

from app import crud
from app.core import config
from app.db.session import db_session
from app.lbsnmodels.user import UserCreate
from app.tests.utils.utils import (
    random_lower_string, random_int, random_fullname, random_name_alias)


def create_random_user(
        origin_id: int = None, user_guid: str = None):
    if origin_id is None:
        origin_id = 0
    if user_guid is None:
        user_guid = random_lower_string()
    user_name = random_name_alias()
    full_name = random_fullname()
    user_in = UserCreate(
        origin_id=origin_id,
        user_guid=user_guid,
        user_name=user_name,
        full_name=full_name)
    return crud.lbsnuser.create(
        db_session=db_session, user_in=user_in
    )
