import requests

from app import crud
from app.core import config
from app.db.session import db_session
from app.lbsnmodels.origin import OriginCreate
from app.tests.utils.utils import random_lower_string, random_int


def create_random_origin(origin_id: int = None):
    if origin_id is None:
        origin_id = random_int()
    name = random_lower_string()
    origin_in = OriginCreate(origin_id=origin_id, name=name)
    return crud.lbsnorigin.create(
        db_session=db_session, origin_in=origin_in
    )
