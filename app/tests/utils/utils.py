import random
import string
from random import randint, uniform
from app.lbsnmodels.utils import Coordinates
import requests

from app.core import config
from faker import Faker
from uuid import uuid4
MyFactory = Faker()


def random_lower_string():
    return "".join(random.choices(string.ascii_lowercase, k=32))


def random_uuid():
    uuid_string = str(uuid4())
    return uuid_string


def random_int():
    return randint(5, 10000)


def random_lat():
    return round(uniform(-90, 90), 15)


def random_lng():
    return round(uniform(-180, 180), 15)


def random_coordinate():
    lng = random_lng()
    lat = random_lat()
    return Coordinates(lng=lng, lat=lat)


def random_fullname():
    return MyFactory.name()  # pylint: disable=no-member


def random_sentence():
    return MyFactory.sentence(nb_words=4)  # pylint: disable=no-member


def random_name_alias():
    fullname = random_fullname()
    name_alias = fullname.replace(" ", "_").lower()
    return name_alias


def get_server_api():
    server_name = f"http://{config.SERVER_NAME}"
    return server_name


def get_superuser_token_headers():
    server_api = get_server_api()
    login_data = {
        "username": config.FIRST_SUPERUSER,
        "password": config.FIRST_SUPERUSER_PASSWORD,
    }
    r = requests.post(
        f"{server_api}{config.API_V1_STR}/login/access-token", data=login_data
    )
    tokens = r.json()
    a_token = tokens["access_token"]
    headers = {"Authorization": f"Bearer {a_token}"}
    # superuser_token_headers = headers
    return headers
