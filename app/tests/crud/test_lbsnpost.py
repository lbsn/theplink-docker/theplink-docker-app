import logging
from pprint import pprint

from fastapi.encoders import jsonable_encoder

from app import crud
from app.db.session import db_session
from app.lbsnmodels.post import PostCreate, PostUpdate, db_post_mapping
from app.tests.utils.utils import (
    MyFactory, random_int, random_lower_string, random_coordinate,
    random_fullname, random_name_alias, random_sentence, random_uuid)

# pylint: disable=maybe-no-member


def test_create_post():
    post_guid = random_uuid()
    post_title = random_lower_string()
    post_body = random_sentence()
    post_location = random_coordinate()
    post_in = PostCreate(
        post_title=post_title,
        post_body=post_body,
        post_latlng_coordinates=post_location)
    post = crud.lbsnpost.create(
        db_session, post_in=post_in, post_guid=post_guid)
    assert post.post_title == post_title
    assert post.post_guid == post_guid
    post_out = db_post_mapping(post)
    assert post_out.post_latlng_coordinates.lng == post_location.lng
    assert post_out.post_latlng_coordinates.lat == post_location.lat


def test_get_post():
    post_guid = random_uuid()
    post_title = random_lower_string()
    post_body = random_sentence()
    post_location = random_coordinate()
    post_in = PostCreate(
        post_title=post_title,
        post_body=post_body,
        post_latlng_coordinates=post_location)
    post = crud.lbsnpost.create(
        db_session, post_in=post_in, post_guid=post_guid)
    stored_post = crud.lbsnpost.get(
        db_session=db_session,
        origin_id=post.origin_id,
        post_guid=post.post_guid)
    assert stored_post
    assert post.origin_id == stored_post.origin_id
    assert post.user_guid == stored_post.user_guid
    assert post.post_title == stored_post.post_title
    post_out = db_post_mapping(post)
    stored_post_out = db_post_mapping(stored_post)
    assert post_out.post_latlng_coordinates.lng == \
        stored_post_out.post_latlng_coordinates.lng
    assert post_out.post_latlng_coordinates.lat == \
        stored_post_out.post_latlng_coordinates.lat


def test_update_post():
    # create initial post data
    post_guid = random_uuid()
    post_title = random_lower_string()
    post_body = random_sentence()
    post_location = random_coordinate()
    post_in = PostCreate(
        post_title=post_title,
        post_body=post_body,
        post_latlng_coordinates=post_location)
    post = crud.lbsnpost.create(
        db_session, post_in=post_in, post_guid=post_guid)
    # create user update data
    post_location2 = random_coordinate()
    post_title2 = random_lower_string()
    post_body2 = random_sentence()
    post_update = PostUpdate(
        post_body=post_body2,
        post_title=post_title2,
        post_latlng_coordinates=post_location2)
    post2 = crud.lbsnpost.update(
        db_session=db_session, post=post,
        post_in=post_update)
    assert post.origin_id == post2.origin_id
    assert post.user_guid == post2.user_guid
    assert post.post_guid == post2.post_guid
    post2_out = db_post_mapping(post2)
    assert post2_out.post_title == post_title2
    assert post2_out.post_body == post_body2
    assert post2_out.post_latlng_coordinates.lat == post_location2.lat
    assert post2_out.post_latlng_coordinates.lng == post_location2.lng
