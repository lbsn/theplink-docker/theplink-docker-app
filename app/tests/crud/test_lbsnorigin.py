from fastapi.encoders import jsonable_encoder

from app import crud
from app.lbsnmodels.origin import OriginCreate, OriginUpdate
from app.tests.utils.utils import random_lower_string, random_int
from app.db.session import db_session


def test_create_origin():
    name = random_lower_string()
    id = random_int()
    origin_in = OriginCreate(origin_id=id, name=name)
    origin = crud.lbsnorigin.create(db_session, origin_in=origin_in)
    assert origin.name == name
    assert origin.origin_id == id


def test_get_origin():
    name = random_lower_string()
    origin_id = random_int()
    origin_in = OriginCreate(origin_id=origin_id, name=name)
    origin = crud.lbsnorigin.create(db_session=db_session, origin_in=origin_in)
    stored_origin = crud.lbsnorigin.get(
        db_session=db_session, origin_id=origin.origin_id)
    assert origin.origin_id == stored_origin.origin_id
    assert origin.name == stored_origin.name


def test_update_origin():
    name = random_lower_string()
    origin_id = random_int()
    origin_in = OriginCreate(origin_id=origin_id, name=name)
    origin = crud.lbsnorigin.create(db_session=db_session, origin_in=origin_in)
    name2 = random_lower_string()
    origin_update = OriginUpdate(name=name2)
    origin2 = crud.lbsnorigin.update(
        db_session=db_session, origin=origin,
        origin_in=origin_update)
    assert origin.origin_id == origin2.origin_id
    assert origin2.name == name2
