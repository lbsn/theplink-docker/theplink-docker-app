import logging
from pprint import pprint

from fastapi.encoders import jsonable_encoder

from app import crud
from app.db.session import db_session
from app.lbsnmodels.user import UserCreate, UserUpdate, db_user_mapping
from app.tests.utils.utils import (
    MyFactory, random_int, random_lower_string, random_coordinate,
    random_fullname, random_name_alias)

# pylint: disable=maybe-no-member


def test_create_user():
    user_name = random_name_alias()
    user_guid = random_lower_string()
    user_location = random_coordinate()
    user_fullname = random_fullname()
    user_in = UserCreate(
        origin_id=0,
        user_guid=user_guid,
        user_name=user_name,
        user_fullname=user_fullname,
        user_location_coordinates=user_location)
    user = crud.lbsnuser.create(db_session, user_in=user_in)
    assert user.user_name == user_name
    assert user.user_guid == user_guid
    user_out = db_user_mapping(user)
    assert user_out.user_location_coordinates.lng == user_location.lng
    assert user_out.user_location_coordinates.lat == user_location.lat


def test_get_user():
    user_name = random_name_alias()
    user_guid = random_lower_string()
    user_location = random_coordinate()
    user_fullname = random_fullname()
    user_in = UserCreate(
        origin_id=0,
        user_guid=user_guid,
        user_name=user_name,
        user_fullname=user_fullname,
        user_location_coordinates=user_location)
    user = crud.lbsnuser.create(db_session, user_in=user_in)
    stored_user = crud.lbsnuser.get(
        db_session=db_session,
        origin_id=user.origin_id,
        user_guid=user.user_guid)
    assert user.origin_id == stored_user.origin_id
    assert user.user_guid == stored_user.user_guid
    assert user.user_name == stored_user.user_name
    user_out = db_user_mapping(user)
    stored_user_out = db_user_mapping(stored_user)
    assert user_out.user_location_coordinates.lng == \
        stored_user_out.user_location_coordinates.lng
    assert user_out.user_location_coordinates.lat == \
        stored_user_out.user_location_coordinates.lat


def test_update_user():
    # create initial user data
    user_name = random_name_alias()
    user_guid = random_lower_string()
    user_location = random_coordinate()
    user_fullname = random_fullname()
    user_in = UserCreate(
        origin_id=0,
        user_guid=user_guid,
        user_name=user_name,
        user_fullname=user_fullname,
        user_location_coordinates=user_location)
    user = crud.lbsnuser.create(db_session, user_in=user_in)
    # create user update data
    user_name2 = random_name_alias()
    user_location2 = random_coordinate()
    user_fullname2 = random_fullname()
    user_update = UserUpdate(
        user_name=user_name2,
        user_fullname=user_fullname2,
        user_location_coordinates=user_location2)
    user2 = crud.lbsnuser.update(
        db_session=db_session, user=user,
        user_in=user_update)
    assert user.origin_id == user2.origin_id
    assert user.user_guid == user2.user_guid
    user2_out = db_user_mapping(user2)
    assert user2_out.user_name == user_name2
    assert user2_out.user_fullname == user_fullname2
    assert user2_out.user_location_coordinates.lat == user_location2.lat
    assert user2_out.user_location_coordinates.lng == user_location2.lng
