import requests

from app.core import config
from app.tests.utils.utils import (
    get_server_api, random_lng, random_lat, random_lower_string,
    random_sentence)
from app.tests.utils.lbsnuser import create_random_user
from app.lbsnmodels.user import UserCreate
import logging


def test_create_user(superuser_token_headers):
    server_api = get_server_api()
    start_id = 5
    start_name = "Foo"
    user_exists = True
    while user_exists:
        data = {
            "origin_id": 0,
            "user_guid": f"{start_id}",
            "user_name": start_name,
            "user_location_coordinates": {
                "lat": random_lat(),
                "lng": random_lng()
            }
        }
        response = requests.post(
            f"{server_api}{config.API_V1_STR}/lbsnusers/",
            headers=superuser_token_headers,
            json=data,
        )
        # duplicate origin_id and user_guid
        # will return 409 (already exists) status code
        if response.status_code != 409:
            user_exists = False
        else:
            # increment id and name to avoid
            # duplicate entries on consecutive tests
            start_id += 1
            start_name = f'{start_name.split("-")[0]}-{start_id}'
    content = response.json()
    assert content["user_name"] == data["user_name"]
    assert content["user_guid"] == data["user_guid"]
    assert content["user_location_coordinates"] == \
        data["user_location_coordinates"]
    assert "user_guid" in content


def test_read_user(superuser_token_headers):

    user = create_random_user()
    server_api = get_server_api()
    response = requests.get(
        f"{server_api}{config.API_V1_STR}/lbsnusers/"
        f"{user.user_guid}?origin_id=0",
        headers=superuser_token_headers,
    )
    content = response.json()
    assert content["user_name"] == user.user_name
    assert content["origin_id"] == user.origin_id
    assert content["user_guid"] == user.user_guid


def test_update_user(superuser_token_headers):

    user = create_random_user()
    server_api = get_server_api()
    data = {
        "user_fullname": random_lower_string(),
        "biography": random_sentence(),
        "user_location_coordinates": {
            "lat": random_lat(),
            "lng": random_lng()
        }
    }
    response = requests.put(
        f"{server_api}{config.API_V1_STR}/lbsnusers/"
        f"{user.user_guid}?origin_id=0",
        headers=superuser_token_headers,
        json=data,
    )
    content = response.json()
    assert content["user_fullname"] == data["user_fullname"]
    assert content["biography"] == data["biography"]
    assert "user_name" in content
    # note: the next line will fail because
    # not submitted data is set to None
    # which will automatically overwrite
    # entries in db:
    # assert content["user_name"] == user.user_name
    assert content["user_location_coordinates"]["lat"] == \
        data["user_location_coordinates"]["lat"]
    assert content["user_location_coordinates"]["lng"] == \
        data["user_location_coordinates"]["lng"]
