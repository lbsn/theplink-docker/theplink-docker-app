import requests

from app.core import config
from app.tests.utils.utils import (
    get_server_api, random_lower_string, random_int, random_sentence, random_coordinate)
from app.tests.utils.lbsnpost import create_random_post
from app.lbsnmodels.post import PostCreate, db_post_mapping
import logging
from pprint import pprint


def test_create_post(superuser_token_headers):
    server_api = get_server_api()
    post_title = random_lower_string()
    post_body = random_sentence()
    post_location = random_coordinate()
    data = {
        "post_title": post_title,
        "post_body": post_body,
        "post_latlng_coordinates": {
            "lat": post_location.lat,
            "lng": post_location.lng
        }
    }
    response = requests.post(
        f"{server_api}{config.API_V1_STR}/lbsnposts/",
        headers=superuser_token_headers,
        json=data,
    )
    content = response.json()
    assert content["post_title"] == data["post_title"]
    assert content["post_body"] == data["post_body"]
    assert "post_guid" in content
    assert content["post_latlng_coordinates"] == \
        data["post_latlng_coordinates"]
    assert "post_guid" in content


def test_read_post():
    post = create_random_post()
    server_api = get_server_api()
    response = requests.get(
        f"{server_api}{config.API_V1_STR}/lbsnposts/"
        f"{post.post_guid}?origin_id=0"
    )
    content = response.json()
    assert content["post_title"] == post.post_title
    assert content["origin_id"] == post.origin_id
    assert content["post_guid"] == post.post_guid
    assert content["post_body"] == post.post_body
    post_out = db_post_mapping(post)
    assert content["post_latlng_coordinates"] == post_out.post_latlng_coordinates


def test_read_map_noauth():
    """Test read post locations for map display"""
    posts = []
    for __ in range(0, 10):
        post = create_random_post()
        post_out = db_post_mapping(post)
        posts.append(post_out)
    server_api = get_server_api()
    response = requests.get(
        f"{server_api}{config.API_V1_STR}/lbsnposts/"
        f"map?origin_id=0",
    )
    content = response.json()
    assert len(content) >= len(posts)
    for post in content:
        for orig_post in posts:
            if post["post_guid"] == orig_post.post_guid:
                assert post["post_latlng_coordinates"]["lng"] == \
                    orig_post.post_latlng_coordinates.lng
                assert post["post_latlng_coordinates"]["lat"] == \
                    orig_post.post_latlng_coordinates.lng
