import requests

from app.core import config
from app.tests.utils.utils import get_server_api
from app.tests.utils.origin import create_random_origin
from app.lbsnmodels.origin import OriginCreate


def test_create_origin(superuser_token_headers):
    server_api = get_server_api()
    start_id = 5
    start_name = "Foo"
    origin_exists = True
    while origin_exists:
        data = {"origin_id": start_id, "name": start_name}
        response = requests.post(
            f"{server_api}{config.API_V1_STR}/origins/",
            headers=superuser_token_headers,
            json=data,
        )
        if response.status_code != 409:
            # both duplicate origin_id and name
            # will return 409 (already exists) status code
            origin_exists = False
        else:
            # increment id and name to avoid
            # duplicate entries on consecutive tests
            start_id += 1
            start_name = f'{start_name.split("_")[0]}_{start_id}'
    content = response.json()
    assert content["name"] == data["name"]
    assert content["origin_id"] == data["origin_id"]
    assert "origin_id" in content


def test_read_origin(superuser_token_headers):

    origin = create_random_origin()
    server_api = get_server_api()
    response = requests.get(
        f"{server_api}{config.API_V1_STR}/origins/{origin.origin_id}",
        headers=superuser_token_headers,
    )
    content = response.json()
    assert content["name"] == origin.name
    assert content["origin_id"] == origin.origin_id
