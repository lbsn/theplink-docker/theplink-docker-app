from fastapi import APIRouter

from app.api.api_v1.endpoints import (
    items, login, users, utils, lbsnorigins, lbsnusers, lbsnposts
)

api_router = APIRouter()
api_router.include_router(
    login.router, tags=["login"])
api_router.include_router(
    users.router, prefix="/users", tags=["users"])
api_router.include_router(
    utils.router, prefix="/utils", tags=["utils"])
api_router.include_router(
    items.router, prefix="/items", tags=["items"])
api_router.include_router(
    lbsnorigins.router, prefix="/origins", tags=["origins"])
api_router.include_router(
    lbsnusers.router, prefix="/lbsnusers", tags=["lbsnusers"])
api_router.include_router(
    lbsnposts.router, prefix="/lbsnposts", tags=["lbsnposts"])
