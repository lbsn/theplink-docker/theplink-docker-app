from typing import List, Tuple
from uuid import uuid4

from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app import crud
from app.api.utils.db import get_db
from app.api.utils.security import (
    get_current_active_superuser, get_current_active_user)
from app.core import config
from app.db_models.user import User as DBUser
from app.lbsnmodels.post import (
    Post, PostCreate, PostUpdate, db_post_mapping,
    db_postlocations_map, PostLocation)
from app.lbsnmodels.utils import uuid_to_guid

router = APIRouter()


@router.get(
    "/", response_model=List[Post], response_model_skip_defaults=True)
def read_posts(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 100,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Retrieve posts.
    """
    if crud.user.is_superuser(current_user):
        posts = crud.lbsnpost.get_multi(db, skip=skip, limit=limit)
    else:
        posts = crud.lbsnpost.get_multi_by_owner(
            db_session=db,
            user_guid=uuid_to_guid(current_user.uuid, current_user.id),
            origin_id=0, skip=skip, limit=limit
        )
    posts_out = [db_post_mapping(post) for post in posts]
    return posts_out

@router.get(
    "/map", response_model=List[PostLocation], response_model_skip_defaults=True)
def read_post_locations(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 1000,
    origin_id: int = 0,
):
    """
    Retrieve post locations for map display (public endpoint).
    """
    post_location_list = crud.lbsnpost.get_multi_map(
        db, skip=skip, limit=limit, origin_id=origin_id)
    post_locations_out = db_postlocations_map(post_location_list)
    return post_locations_out

@router.post(
    "/", response_model=Post, response_model_skip_defaults=True)
def create_post(
    *,
    db: Session = Depends(get_db),
    post_in: PostCreate,
    post_guid: str = None,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Create new post.
    """
    if post_guid and not crud.user.is_superuser(current_user):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    if not post_guid:
        post_guid = str(uuid4())

    post = crud.lbsnpost.get(
        db_session=db,
        origin_id=0,
        post_guid=post_guid)
    if post:
        raise HTTPException(
            status_code=409, detail="Post id already exists")
    post = crud.lbsnpost.create(
        db_session=db,
        post_in=post_in,
        post_guid=post_guid,
        user_guid=uuid_to_guid(current_user.uuid, current_user.id))
    # return response to user
    # (with coordinates, not geom)
    post_out = db_post_mapping(post)
    return post_out


@router.put(
    "/{post_guid}", response_model=Post,
    response_model_skip_defaults=True)
def update_post(
    *,
    db: Session = Depends(get_db),
    post_guid: str,
    post_in: PostUpdate,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Update a post (own posts only).
    """
    post_db = crud.lbsnpost.get(
        db_session=db,
        origin_id=0,
        post_guid=post_guid)
    if post_db.user_guid != uuid_to_guid(current_user.uuid, current_user.id):
        raise HTTPException(
            status_code=400, detail="You can only update your own posts")
    if not post_db:
        raise HTTPException(
            status_code=404, detail="Post not found")
    post = crud.lbsnpost.update(
        db_session=db, post=post_db, post_in=post_in)
    # return response to user
    # (with coordinates, not geom)
    post_out = db_post_mapping(post)
    return post_out


@router.get(
    "/{post_guid}", response_model=Post,
    response_model_skip_defaults=True)
def get_post(
    *,
    db: Session = Depends(get_db),
    post_guid: str,
    origin_id: int = 0,
):
    """
    Get post by ID (public endpoint).
    """
    post = crud.lbsnpost.get(
        db_session=db,
        origin_id=origin_id,
        post_guid=post_guid)
    if not post:
        raise HTTPException(status_code=400, detail="Post not found")
    post_out = db_post_mapping(post)
    return post_out
