from typing import List

from fastapi import APIRouter, Body, Depends, HTTPException, Path
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app import crud
from app.api.utils.db import get_db
from app.api.utils.security import get_current_active_superuser, get_current_active_user
from app.core import config
from app.db_models.user import User as DBUser
from app.db_models.lbsn import Origin as DBOrigin
from app.lbsnmodels.origin import Origin, OriginCreate, OriginInDB, OriginUpdate, ORIGIN_ID_SCHEMA


router = APIRouter()

ORIGIN_ID_PATH = Path(
    default=ORIGIN_ID_SCHEMA.default,
    description=ORIGIN_ID_SCHEMA.description,
    min_length=ORIGIN_ID_SCHEMA.min_length,
    max_length=ORIGIN_ID_SCHEMA.max_length
)


@router.get("/", response_model=List[Origin])
def read_origins(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 100,
    current_user: DBUser = Depends(get_current_active_superuser),
):
    """
    Retrieve origins (superusers only).
    """
    origins = crud.lbsnorigin.get_multi(db, skip=skip, limit=limit)
    return origins


@router.post("/", response_model=Origin)
def create_origin(
    *,
    db: Session = Depends(get_db),
    origin_in: OriginCreate,
    current_user: DBUser = Depends(get_current_active_superuser),
):
    """
    Create new origin (superusers only).
    """
    origin = crud.lbsnorigin.get(
        db_session=db, origin_id=origin_in.origin_id)
    if origin:
        raise HTTPException(
            status_code=409, detail="Origin already exists")
    origin = crud.lbsnorigin.get_by_name(db, name=origin_in.name)
    if origin:
        raise HTTPException(
            status_code=409,
            detail=("An origin with this name "
                    "already exists in the system"),
        )
    origin = crud.lbsnorigin.create(db_session=db, origin_in=origin_in)
    return origin


@router.put("/{origin_id}", response_model=Origin)
def update_origin(
    *,
    db: Session = Depends(get_db),
    origin_id: int = ORIGIN_ID_PATH,
    origin_in: OriginUpdate,
    current_user: DBUser = Depends(get_current_active_superuser),
):
    """
    Update an origin (superusers only).
    """
    origin = crud.lbsnorigin.get(db_session=db, origin_id=origin_id)
    if not origin:
        raise HTTPException(status_code=404, detail="Origin not found")
    origin = crud.lbsnorigin.update(
        db_session=db, origin=origin, origin_in=origin_in)
    return origin


@router.get("/{origin_id}", response_model=Origin)
def get_origin(
    *,
    db: Session = Depends(get_db),
    origin_id: int = ORIGIN_ID_PATH,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Get origin by ID.
    """
    origin = crud.lbsnorigin.get(db_session=db, origin_id=origin_id)
    if not origin:
        raise HTTPException(status_code=400, detail="Origin not found")
    return origin
