from typing import List

from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app import crud
from app.api.utils.db import get_db
from app.api.utils.security import get_current_active_superuser, get_current_active_user
from app.core import config
from app.db_models.user import User as DBUser
from app.lbsnmodels.user import (
    User, UserCreate, UserUpdate, db_user_mapping)

router = APIRouter()


@router.get(
    "/", response_model=List[User], response_model_skip_defaults=True)
def read_users(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 100,
    current_user: DBUser = Depends(get_current_active_superuser),
):
    """
    Retrieve users (superusers only).
    """
    users = crud.lbsnuser.get_multi(db, skip=skip, limit=limit)
    users_out = [db_user_mapping(user) for user in users]
    return users_out


@router.post(
    "/", response_model=User, response_model_skip_defaults=True)
def create_user(
    *,
    db: Session = Depends(get_db),
    user_in: UserCreate,
    current_user: DBUser = Depends(get_current_active_superuser),
):
    """
    Create new user (superusers only).
    """
    user = crud.lbsnuser.get(
        db_session=db, user_guid=user_in.user_guid,
        origin_id=user_in.origin_id)
    if user:
        raise HTTPException(
            status_code=409, detail="User id already exists")
    user = crud.lbsnuser.create(db_session=db, user_in=user_in)
    # return response to user
    # (with coordinates, not geom)
    user_out = db_user_mapping(user)
    return user_out


@router.put(
    "/{user_guid}", response_model=User,
    response_model_skip_defaults=True)
def update_user(
    *,
    db: Session = Depends(get_db),
    user_guid: str,
    origin_id: int = None,
    user_in: UserUpdate,
    current_user: DBUser = Depends(get_current_active_superuser),
):
    """
    Update a user (superusers only).
    """
    user_db = crud.lbsnuser.get(
        db_session=db,
        user_guid=user_guid,
        origin_id=origin_id)
    if not user_db:
        raise HTTPException(
            status_code=404, detail="User not found")
    user = crud.lbsnuser.update(
        db_session=db, user=user_db, user_in=user_in)
    # return response to user
    # (with coordinates, not geom)
    user_out = db_user_mapping(user)
    return user_out


@router.get(
    "/{user_guid}", response_model=User,
    response_model_skip_defaults=True)
def get_user(
    *,
    db: Session = Depends(get_db),
    user_guid: str,
    origin_id: int = 0,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Get user by ID.
    """
    user = crud.lbsnuser.get(
        db_session=db, user_guid=user_guid,
        origin_id=origin_id)
    if not user:
        raise HTTPException(status_code=400, detail="User not found")
    user_out = db_user_mapping(user)
    return user_out
