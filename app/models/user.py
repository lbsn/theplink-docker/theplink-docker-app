from typing import Optional

from pydantic import BaseModel, UUID4


class UserBase(BaseModel):
    """Shared properties"""
    email: Optional[str] = None
    is_active: Optional[bool] = True
    is_superuser: Optional[bool] = False
    full_name: Optional[str] = None

    class Config:
        orm_mode = True


class UserBaseInDB(UserBase):
    id: int = None


class UserCreate(UserBaseInDB):
    """Properties to receive via API on creation"""
    email: str
    password: str


class UserUpdate(UserBaseInDB):
    """Properties to receive via API on update"""
    password: Optional[str] = None


class User(UserBaseInDB):
    """Additional properties to return via API"""
    pass


class UserInDB(UserBaseInDB):
    """Additional properties stored in DB"""
    hashed_password: str
    uuid: UUID4
