from pydantic import BaseModel


class ItemBase(BaseModel):
    """Shared properties"""
    title: str = None
    description: str = None

    class Config:
        orm_mode = True


class ItemCreate(ItemBase):
    """Properties to receive on item creation"""
    title: str


class ItemUpdate(ItemBase):
    """Properties to receive on item update"""
    pass


class ItemInDBBase(ItemBase):
    """Properties shared by models stored in DB"""
    id: int
    title: str
    owner_id: int


class Item(ItemInDBBase):
    """Properties to return to client"""
    pass


class ItemInDB(ItemInDBBase):
    """Properties properties stored in DB"""
    pass
