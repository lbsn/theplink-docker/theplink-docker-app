# Import all the models, so that Base has them before being
# imported by Alembic
from app.db.base_class import LBSNBase
from app.db_models.lbsn import (
    Origin, Language, Country, City, Place,
    User, UserGroup, Post, PostReaction, Term)
