from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy import MetaData


class CustomBase(object):
    # Generate __tablename__ automatically
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()


Base = declarative_base(cls=CustomBase)
LBSNBase = declarative_base(cls=CustomBase, metadata=MetaData(schema='data'))
