from app import crud
from app.core import config
from app.lbsnmodels.origin import OriginCreate
from app.lbsnmodels.user import UserCreate
from app.lbsnmodels.utils import email_to_username, uuid_to_guid

# make sure all SQL Alchemy models are imported before initializing DB
# otherwise, SQL Alchemy might fail to initialize properly relationships
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28
from app.db import lbsn_base


def init_lbsn_db(db_session):
    # Tables should be created with Alembic migrations
    # But if you don't want to use migrations, create
    # the tables un-commenting the next line
    # Base.metadata.create_all(bind=engine)

    # Create Origin 0
    origin = crud.lbsnorigin.get(
        db_session=db_session, origin_id=0)
    if not origin:
        origin_in = OriginCreate(
            origin_id=0,
            name=config.PROJECT_NAME
        )
        origin = crud.lbsnorigin.create(db_session, origin_in=origin_in)
    # Create first LBSN User
    user = crud.user.get_by_email(db_session, email=config.FIRST_SUPERUSER)
    user_guid = uuid_to_guid(user.uuid, user.id)
    lbsn_user = crud.lbsnuser.get(
        db_session, user_guid=user_guid, origin_id=0)
    if not lbsn_user:
        lbsn_user_in = UserCreate(
            origin_id=0,
            user_guid=user_guid,
            user_fullname=user.full_name,
            user_name=email_to_username(user.email))
        lbsn_user = crud.lbsnuser.create(db_session, user_in=lbsn_user_in)
