from typing import Optional
from app.core.config import PROJECT_NAME
from pydantic import BaseModel, Schema

ORIGIN_ID_SCHEMA = Schema(
    default=...,
    description=(f"A unique Origin ID. Default origin id for "
                 f"{PROJECT_NAME} (this service) is 0"),
    min_length=2,
    max_length=100,
    example="1"
)
ORIGIN_NAME_SCHEMA = Schema(
    ...,
    description="The name of Origin, e.g. the service name",
    min_length=2,
    max_length=100,
    example="Flickr YFCC100m"
)


class OriginBase(BaseModel):
    """Shared properties"""
    name: str = ORIGIN_NAME_SCHEMA


class OriginCreate(OriginBase):
    """Properties to receive on Origin creation"""
    origin_id: int = ORIGIN_ID_SCHEMA


class OriginUpdate(OriginBase):
    """Properties to receive on Origin update
    """
    pass


class OriginBaseInDB(OriginBase):
    """Properties shared by models stored in DB"""
    origin_id: int
    name: str


class Origin(OriginBaseInDB):
    """Properties to return to client"""
    pass


class OriginInDB(OriginBaseInDB):
    """Properties stored in DB"""
    pass
