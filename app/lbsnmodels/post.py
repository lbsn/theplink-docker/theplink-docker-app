from datetime import datetime
from typing import Optional, Set, List, Tuple

from fastapi.encoders import jsonable_encoder
from geoalchemy2.shape import to_shape, WKBElement
from pydantic import BaseModel, Schema, validator
from shapely import wkt

from app.db_models.lbsn import Post as DBPost
from app.lbsnmodels.utils import (NULL_ISLAND_EWKT, Coordinates, Geoaccuracy,
                                  PostType, get_coordinates_from_ewkt,
                                  get_ewkt_from_coordinates,
                                  get_geom_from_ewkb,
                                  get_coordinates_from_geompoint,
                                  get_wkt_from_geompoint,
                                  get_geom_from_ewkb_ewkt)

POST_LOCATIONGEOM_SCHEMA = Schema(
    None,
    description=(
        "Location of the post, either chosen by the user, automatically "
        "attched to the post by input device (GPS) or complemented "
        "by algorithms of the service (e.g. Twitter post geocoding, as derived "
        "from the post_body text. If lat/lng coordinates are not submitted, "
        "this field can be substituded with location information from place, "
        "city or country table. In those cases, post_geoaccuracy indicates "
        "lower level geoaccuracy, e.g.: 'place', 'city', or 'country'. "
        "Formatting: WKT (Well-Known-Text String)"),
    example=Coordinates(lng=44.213, lat=-29.457)
)
POST_GEOACCURACY_SCHEMA = Schema(
    Geoaccuracy("unknown"),
    description=(
        "Specifies highest location accuracy available, "
        "either 'latlng', 'place', 'city' or 'country'."),
    example="latlng"
)
POST_COMMENTCOUNT_SCHEMA = Schema(
    None,
    description=(
        "Number of times this Post has been commented "
        "by other users, e.g. count of Reply-Tweets on Twitter, "
        "count of comments on Flickr etc."),
    example=75
)
POST_QUOTECOUNT_SCHEMA = Schema(
    None,
    description=(
        "Number of times this Post has been quoted by other users, "
        "e.g. count of Quote-Tweets on Twitter."),
    example=75
)
POST_SHARECOUNT_SCHEMA = Schema(
    None,
    description=(
        "Number of times this Post has been shared "
        "by other users, e.g. count of Retweets on Twitter."),
    example=75
)
POST_TYPE_SCHEMA = Schema(
    PostType("text"),
    description=(
        "Type of post, e.g. text, image, video or other. "
        "Choose the more specific type "
        "(VIDEO over TEXT even if text is present)."),
    example="image"
)
POST_FILTER_SCHEMA = Schema(
    None,
    description=(
        "Any automatic filters applied to post? "
        "(e.g. photo filters such as Amarao; "
        "Automatic Translations of Text)."),
    example=""
)
POST_INPUTSOURCE_SCHEMA = Schema(
    None,
    description=(
        "Type of input device used by the user to post, "
        "for a list see Twitter, e.g. 'Web', 'IPhone', "
        "'Android' etc. Recommendation: should be oriented "
        "at Twitter's large list of source types. For camera"
        "models, have a look at Flickr."),
    example="Mobile Phone"
)
POST_USERMENTIONS_SCHEMA = Schema(
    [],
    description=(
        "An array consisting of user_guids that are "
        "mentioned in the post_body, post_title or "
        "other parts of a post. These are not direct "
        "references checked by postgres, but mere lists, "
        "since Foreign Key Arrays are currently not supported."),
    example=["foo_fighter", "foo"]
)
POST_CONTENTLICENSE_SCHEMA = Schema(
    None,
    description=(
        "An integer for specifying the of the post "
        "which can be optionally chosen by users on "
        "some services (e.g. Flickr). For example: "
        "All Rights Reserved = 0. Numbers can be "
        "oriented at Flickr's list of content licenses: "
        "https://www.flickr.com/services/api/flickr.photos.licenses.getInfo.html"),
    example=2
)


class PostBase(BaseModel):
    """Shared properties (can appear everywhere)"""
    place_guid: str = None
    city_guid: str = None
    country_guid: str = None
    post_body: str = None
    post_geoaccuracy: Optional[Geoaccuracy] = POST_GEOACCURACY_SCHEMA
    hashtags: Set[str] = []
    emoji: Set[str] = []
    post_title: str = None
    post_create_date: datetime = None
    post_thumbnail_url: str = None
    post_type: Optional[PostType] = POST_TYPE_SCHEMA
    post_filter: str = POST_FILTER_SCHEMA
    # post_language: str = POST_LANGUAGE_SCHEMA
    input_source: str = POST_INPUTSOURCE_SCHEMA
    post_content_license: int = POST_CONTENTLICENSE_SCHEMA


class PostBaseInDB(PostBase):
    """Properties shared by models stored in DB"""
    origin_id: int = 0
    post_guid: str
    user_guid: str = None
    modified: datetime = None
    post_like_count: int = None
    post_comment_count: int = POST_COMMENTCOUNT_SCHEMA
    post_views_count: int = None
    post_url: str = None
    post_quote_count: int = POST_QUOTECOUNT_SCHEMA
    post_share_count: int = POST_SHARECOUNT_SCHEMA
    user_mentions: Set[str] = POST_USERMENTIONS_SCHEMA
    post_publish_date: datetime = None

    @validator('post_publish_date', pre=True, always=True)
    def set_ts_now(cls, v):
        return v or datetime.now()


class PostCreate(PostBase):
    """Properties to receive on Post creation"""
    post_latlng_coordinates: Coordinates = POST_LOCATIONGEOM_SCHEMA


class PostUpdate(PostBase):
    """Properties to receive on Post update"""
    post_latlng_coordinates: Coordinates = POST_LOCATIONGEOM_SCHEMA


class Post(PostBaseInDB):
    """Properties to return to client"""
    post_latlng_coordinates: Coordinates = POST_LOCATIONGEOM_SCHEMA


class PostInDB(PostBaseInDB):
    """Properties properties stored in DB"""
    post_latlng: str = NULL_ISLAND_EWKT


class PostLocation(BaseModel):
    """Properties properties stored in DB"""
    post_latlng_coordinates: Coordinates = POST_LOCATIONGEOM_SCHEMA
    post_guid: str


def post_postindb_mapping(
        post_in: [PostCreate, PostUpdate, Post],
        user_guid: str = None,
        post_guid: str = None) -> Optional[PostInDB]:
    """Convert lat/lng to geom (ewkt-string)
    for storing to db
    """
    post_latlng = get_ewkt_from_coordinates(
        post_in.post_latlng_coordinates)
    # map input to UserInDB
    post = PostInDB(
        **post_in.dict(),
        post_latlng=post_latlng, user_guid=user_guid, post_guid=post_guid)
    return post


def postindb_post_mapping(
        post_db: PostInDB) -> Optional[Post]:
    """For returning information back to user,
    map PostInDB back to Post (output)
    first, convert geom (ewkt-string) to coordinates
    """
    coordinates = get_coordinates_from_ewkt(
        post_db.post_latlng)
    post = Post(
        **post_db.dict(), post_latlng_coordinates=coordinates)
    return post


def postindb_db_mapping(postindb: PostInDB) -> DBPost:
    """Maps data to db structure"""
    postindb_data = jsonable_encoder(postindb)
    post_db = DBPost(**postindb_data)
    return post_db


def db_postindb_mapping(post_db: DBPost) -> PostInDB:
    """Maps data from db structure
    """
    # EWKBElement
    post_location_ewkb = post_db.post_latlng
    # EWKBElement -> Geometry (Point)
    post_location_pointgeom = to_shape(post_location_ewkb)
    # EWKBElement -> WKTE (WKT with SRID)
    post_db.post_latlng = (
        f'SRID=4326;{wkt.dumps(post_location_pointgeom)}')
    # get data as json-dict
    post_data = jsonable_encoder(post_db)
    # perform mapping to pydantic model
    postindb = PostInDB(**post_data)
    return postindb


def db_post_mapping(post_db: DBPost) -> Post:
    """Convert db structure directly to Post Pydantic model
    """
    # eWKB or eWKT?
    post_location_pointgeom = get_geom_from_ewkb_ewkt(
        post_db.post_latlng)
    post_latlng_coordinates = get_coordinates_from_geompoint(
        post_location_pointgeom)
    # jsonable_encoder doesn't support WKBElement
    # (ORM Format from Geoalchemy2)
    # overwrite with WKT string
    # before doing the mapping
    post_db.post_latlng = get_wkt_from_geompoint(
        post_location_pointgeom, extended=True)
    # get data as json-dict
    post_data = jsonable_encoder(post_db)
    # perform mapping to pydantic model
    post = Post(
        **post_data,
        post_latlng_coordinates=post_latlng_coordinates)
    return post


def db_postlocations_map(
    post_map_db: List[Tuple[WKBElement, str]]
) -> List[PostLocation]:
    """Convert db structure (map + guid) directly to Post Pydantic model
    """
    posts = []
    for location_guid_tuple in post_map_db:
        post_location_pointgeom = get_geom_from_ewkb_ewkt(
            location_guid_tuple[0])
        post_latlng_coordinates = get_coordinates_from_geompoint(
            post_location_pointgeom)
        post = PostLocation(
            post_latlng_coordinates=post_latlng_coordinates,
            post_guid=location_guid_tuple[1])
        posts.append(post)
    return posts
