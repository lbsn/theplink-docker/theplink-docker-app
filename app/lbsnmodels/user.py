from datetime import datetime
from typing import List, Optional, Union

from fastapi.encoders import jsonable_encoder
from geoalchemy2.shape import to_shape, WKBElement
from pydantic import BaseModel, Schema
from shapely import wkt

from app.db_models.lbsn import User as DBUser
from app.lbsnmodels.utils import (NULL_ISLAND_EWKT, Coordinates,
                                  get_coordinates_from_ewkt,
                                  get_ewkt_from_coordinates,
                                  get_geom_from_ewkt, get_geom_from_ewkb,
                                  get_coordinates_from_geompoint,
                                  get_wkt_from_geompoint,
                                  get_geom_from_ewkb_ewkt)

USER_NAME_SCHEMA = Schema(
    None,
    description=(
        "Name of the User. "
        "Can be an alias, email or real name etc."),
    min_length=2,
    max_length=100,
    example="foo_fighter"
)
USER_FULLNAME_SCHEMA = Schema(
    None,
    description=(
        "Full name of the User. "
        "Can be an alias or real name etc."),
    min_length=2,
    max_length=100,
    example="Foo Fighter"
)
USER_FOLLOWS_SCHEMA = Schema(
    None,
    description="Number of other users this user follows.",
    example=75
)
USER_FOLLOWED_SCHEMA = Schema(
    None,
    description="Number of times this user is followed by others.",
    example=5
)
USER_BIO_SCHEMA = Schema(
    None,
    description="A short user biography or description.",
    max_length=5000,
    example="Foo ❤ Fighter"
)
USER_POSTCOUNT_SCHEMA = Schema(
    None,
    description="Number of posts this user has created.",
    example=33
)
USER_URL_SCHEMA = Schema(
    None,
    description="Full URL to user profile.",
    example=""
)
USER_ISPRIVATE_SCHEMA = Schema(
    None,
    description="Whether the user is private or not.",
    example=False
)
USER_ISAVAILABLE_SCHEMA = Schema(
    None,
    description=(
        "When the user's account was deactivated or when "
        "users explicitly choOse to delete their account, "
        "but keep public data, this field would be False."),
    example=True
)
USER_GROUPCOUNT_SCHEMA = Schema(
    None,
    description=(
        "The number of public groups "
        "or communities this user is part of."),
    example=2
)
# USER_LANGUAGE_SCHEMA = Schema(
#     None,
#     description=(
#         "A BCP 47 language identifier corresponding to the "
#         "machine-detected or user defined language."),
#     example="en-US"
# )


USER_LOCATION_SCHEMA = Schema(
    None,
    description=(
        "The user-defined location for this profile. "
        "Not necessarily a location, nor machine-parseable."),
    example="Paris"
)
USER_LOCATIONGEOM_SCHEMA = Schema(
    None,
    description=(
        "Coordinates (Point: lat/lng) "
        "of the user-location, either provided by user "
        "or geocoded from the user's location."),
    example=Coordinates(lng=12.122, lat=5.134)
)
USER_LIKEDCOUNT_SCHEMA = Schema(
    None,
    description=(
        "The number of Posts this user has liked in total."),
    example=789
)
USER_ACTIVESINCE_SCHEMA = Schema(
    None,
    description=(
        "UTC datetime when the user was first active "
        "(e.g. time of account creation, "
        "or derived from first post_publish_date)."),
    example=datetime.now()
)
USER_PROFILEIMGURL_SCHEMA = Schema(
    None,
    description=(
        "URL pointing to the profile image of the user."),
    example=""
)
USER_TIMEZONE_SCHEMA = Schema(
    None,
    description=(
        "Time zone ID that can be specified by the user."),
    example="Pacific/Honolulu"
)
USER_UTCOFFSET_SCHEMA = Schema(
    None,
    description=(
        "Optional difference in hours "
        "from Coordinated Universal Time (UTC) "
        "for a particular user defined place."),
    example=-11
)

USER_GROUPSMEMBER_SCHEMA = Schema(
    None,
    description=(
        "The list of groups this user has joined/ is a "
        "member of (active participation interest)."),
    example=["Foo Groupies", "Fighters Club"]
)
USER_GROUPSFOLLOWS_SCHEMA = Schema(
    None,
    description=(
        "The list of groups this user follows (viewing interest)."),
    example=["Foo ❤ Fighters", "Weather Channel"]
)


class UserBase(BaseModel):
    """Shared properties (can appear everywhere)"""
    user_name: str = USER_NAME_SCHEMA
    user_fullname: str = USER_FULLNAME_SCHEMA
    biography: str = USER_BIO_SCHEMA
    url: str = USER_URL_SCHEMA
    is_private: bool = USER_ISPRIVATE_SCHEMA
    is_available: bool = USER_ISAVAILABLE_SCHEMA
    # user_language: str = USER_LANGUAGE_SCHEMA
    user_location: str = USER_LOCATION_SCHEMA
    profile_image_url: str = USER_PROFILEIMGURL_SCHEMA
    user_timezone: str = USER_TIMEZONE_SCHEMA
    user_utc_offset: int = USER_UTCOFFSET_SCHEMA


class UserBaseInDB(UserBase):
    """Properties shared by models stored in DB"""
    origin_id: int = None
    user_guid: str = None
    modified: datetime = datetime.now()
    follows: int = USER_FOLLOWS_SCHEMA
    followed: int = USER_FOLLOWED_SCHEMA
    post_count: int = USER_POSTCOUNT_SCHEMA
    liked_count: int = USER_LIKEDCOUNT_SCHEMA
    active_since: datetime = USER_ACTIVESINCE_SCHEMA
    user_groups_member: List[str] = USER_GROUPSMEMBER_SCHEMA
    user_groups_follows: List[str] = USER_GROUPSFOLLOWS_SCHEMA
    group_count: int = USER_GROUPCOUNT_SCHEMA


class UserCreate(UserBase):
    """Properties to receive on User creation"""
    origin_id: int = 0
    user_guid: str
    user_location_coordinates: Coordinates = USER_LOCATIONGEOM_SCHEMA


class UserUpdate(UserBase):
    """Properties to receive on User update"""
    user_location_coordinates: Coordinates = USER_LOCATIONGEOM_SCHEMA


class User(UserBaseInDB):
    """Properties to return to client"""
    user_location_coordinates: Coordinates = USER_LOCATIONGEOM_SCHEMA


class UserInDB(UserBaseInDB):
    """Properties properties stored in DB"""
    user_location_geom: str = NULL_ISLAND_EWKT


def user_userindb_mapping(
        user_in: Union[UserCreate, UserUpdate, User]) -> Optional[UserInDB]:
    """convert lat/lng to geom (ewkt-string)
    for storing to db
    """

    user_location_geom = get_ewkt_from_coordinates(
        user_in.user_location_coordinates)
    # map input to UserInDB
    user = UserInDB(
        **user_in.dict(), user_location_geom=user_location_geom)
    return user


def userindb_user_mapping(
        user_db: UserInDB) -> Optional[User]:
    """For returning information back to user,
    map UserInDB back to User (output)
    first, convert geom (ewkt-string) to coordinates
    """
    coordinates = get_coordinates_from_ewkt(
        user_db.user_location_geom)
    user = User(
        **user_db.dict(), user_location_coordinates=coordinates)
    return user


def userindb_db_mapping(userindb: UserInDB) -> DBUser:
    """Maps data to db structure"""
    userindb_data = jsonable_encoder(userindb)
    user_db = DBUser(**userindb_data)
    return user_db


def db_userindb_mapping(user_db: DBUser) -> UserInDB:
    """Maps data from db structure

    Note: Geoalchemy2 WKBElement in field user_location_geom
    needs manual conversion because it is not supported
    by jsonable_encoder. Furthermore, SRID is not returned
    from Geoalchemy2 geom field query, add manually
    """
    # EWKBElement
    user_location_ewkb = user_db.user_location_geom
    # EWKBElement -> Geometry (Point)
    user_location_pointgeom = to_shape(user_location_ewkb)
    # EWKBElement -> WKTE (WKT with SRID)
    user_db.user_location_geom = (
        f'SRID=4326;{wkt.dumps(user_location_pointgeom)}')
    # get data as json-dict
    user_data = jsonable_encoder(user_db)
    # perform mapping to pydantic model
    userindb = UserInDB(**user_data)
    return userindb


def db_user_mapping(user_db: DBUser) -> User:
    """Convert db structure directly to User Pydantic model
    """
    # eWKB or eWKT?
    user_location_pointgeom = get_geom_from_ewkb_ewkt(
        user_db.user_location_geom)
    user_location_coordinates = get_coordinates_from_geompoint(
        user_location_pointgeom)
    # jsonable_encoder doesn't support WKBElement
    # (ORM Format from Geoalchemy2)
    # overwrite with WKT string
    # before doing the mapping
    user_db.user_location_geom = get_wkt_from_geompoint(
        user_location_pointgeom, extended=True)
    # get data as json-dict
    user_data = jsonable_encoder(user_db)
    # perform mapping to pydantic model
    user = User(
        **user_data,
        user_location_coordinates=user_location_coordinates)
    return user
