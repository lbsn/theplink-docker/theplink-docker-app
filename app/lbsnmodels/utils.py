import logging

from geoalchemy2.elements import WKBElement
from geoalchemy2.shape import to_shape
from pydantic import BaseModel, validator, UUID4
from shapely import wkt, wkb
from shapely.geometry import Point, Polygon
from typing import Union
from enum import Enum
from uuid import uuid3


class Geoaccuracy(str, Enum):
    unknown = "unknown"
    latlng = "latlng"
    place = "place"
    city = "city"
    country = "country"


class PostType(str, Enum):
    other = "other"
    text = "text"
    image = "image"
    video = "video"


class Coordinates(BaseModel):
    lat: float = 0
    lng: float = 0

    @validator('lat')
    def lat_within_range(cls, v):
        if not -90 <= v <= 90:
            raise ValueError('Latitude outside allowed range')
        return v

    @validator('lng')
    def lng_within_range(cls, v):
        if not -180 <= v <= 180:
            raise ValueError('Longitude outside allowed range')
        return v


# PointCoordinates(0, 0)
NULL_ISLAND_COORDINATE = Coordinates(
    lng=0, lat=0)


def get_geompoint_from_coordinates(
        coordinates: Coordinates) -> Point:
    """Construct shapely Point geometry from coordinate"""
    geom_point = Point(coordinates.lng, coordinates.lat)
    return geom_point


def get_coordinates_from_geompoint(
        geom_point: Point) -> Coordinates:
    """Construct coordinates (Point) from shapely Point geometry"""
    coordinates = Coordinates(lng=geom_point.x, lat=geom_point.y)
    return coordinates


# Point(0, 0)
NULL_ISLAND_GEOM = get_geompoint_from_coordinates(
    NULL_ISLAND_COORDINATE)


def reduce_ewkt_to_wkt(geom_ewkt: str) -> str:
    """Hack to reduce extended WKT (eWKT) to WKT"""
    geom_wkt = geom_ewkt.replace("SRID=4326;", "")
    return geom_wkt


def upgrade_wkt_to_ewkt(geom_wkt: str) -> str:
    """Hack to upgrade WKT to extended eWKT"""
    geom_ewkt = f'SRID=4326;{geom_wkt}'
    return geom_ewkt


def get_wkt_from_geompoint(
        geom_point: Point, extended: bool = None) -> str:
    """Construct Well-Known-Text (WKE) from shapely Point geometry

    Attributes:
        extended: if True, add SRID=4326 (WGS1984) identifier
    """
    if extended is None:
        extended = False
    point_wkt = wkt.dumps(geom_point)
    if extended:
        point_ewkt = upgrade_wkt_to_ewkt(point_wkt)
        return point_ewkt
    else:
        return point_wkt


# SRID=4326;Point(0, 0)
NULL_ISLAND_EWKT = get_wkt_from_geompoint(
    NULL_ISLAND_GEOM, extended=True)


def get_ewkt_from_coordinates(
        coordinates: Coordinates) -> str:
    """Construct extended WKT

    TODO: use shapely.wkb.dumps, which
    accepts, as of 1.7a1, an srid integer
    keyword argument to write WKB data
    including a spatial reference ID
    in the output data
    """
    if not coordinates:
        return NULL_ISLAND_EWKT
    geom_point = get_geompoint_from_coordinates(
        coordinates)
    geom_point_ewkt = get_wkt_from_geompoint(
        geom_point, extended=True)
    # geom_wkte = f'SRID=4326;POINT({coordinates.lng} {coordinates.lat})'
    return geom_point_ewkt


def get_geom_from_ewkt(
        geom_ewkt: str) -> Union[Point, Polygon]:
    """Convert EWKT representation (without srid) to shapely geometry

    Note: either Point or Polygon
    """
    geom_wkt = reduce_ewkt_to_wkt(geom_ewkt)
    shply_geom = wkt.loads(geom_wkt)
    return shply_geom


def get_coordinates_from_ewkt(
        geom: str) -> Coordinates:
    """Convert EWKT representation (with srid) to geometry

    Note:
    Shapely has no support for handling SRID (projection). The
    approach used here is a shortcut. This should be replaced
    by proper EWKT handling using a package, e.g.
    django.contrib.gis.geos or django.contrib.gis.geometry, see:
    https://docs.huihoo.com/django/1.11/ref/contrib/gis/geos.html
    """
    if not geom:
        return Coordinates()
    geom = reduce_ewkt_to_wkt(geom)
    shply_geom = get_geom_from_ewkt(geom)
    if not shply_geom.geom_type == "Point":
        raise ValueError(
            f"Expected geometry of type Point, "
            f"but found {shply_geom.geom_type}")
    coordinates = Coordinates(
        lng=shply_geom.x, lat=shply_geom.y)  # pylint: disable=maybe-no-member
    return coordinates


def get_geom_from_ewkb(
        geom: WKBElement) -> Point:
    """Convert EWKB representation (with srid) to Point geometry"""
    if not isinstance(geom, WKBElement):
        raise ValueError(f"Expected WKBElement, got {type(geom)}")
    shply_geom = to_shape(geom)
    if not shply_geom.geom_type == "Point":
        raise ValueError(
            f"Expected geometry of type Point, "
            f"got {shply_geom.geom_type}")
    return shply_geom


def get_geom_from_ewkb_ewkt(
        geom: Union[WKBElement, str]) -> Point:
    """Detect whether input is (e)WKB or (e)WKT
    and convert to point geometry

    TODO: The reason why type detection is necessary
    is that commit(db_user) needs WKT, but read(db_user)
    returns WKB. This issue originates from geoalchemy2
    and can likely be fixed with the SQLAlchemy
    Custom SQL Constructs and Compilation Extension, see:
    https://docs.sqlalchemy.org/en/13/core/compiler.html
    """
    if geom is None:
        logging.getLogger().warning("Empty Geometry detected")
        return NULL_ISLAND_GEOM
    elif isinstance(geom, WKBElement):
        # logging.getLogger().info("Detected WKBElement")
        pointgeom = get_geom_from_ewkb(
            geom)
    elif isinstance(geom, str):
        # logging.getLogger().info("Detected WKTElement")
        pointgeom = get_geom_from_ewkt(
            geom)
    else:
        raise ValueError(
            f"Expected str or WKBElement, got {type(geom)}, value: {geom}")
    return pointgeom


def email_to_username(email: str = None) -> str:
    user_name = email.replace("@", "_").split(".")[0]
    return user_name


def uuid_to_guid(uuid: UUID4 = None, id: int = None) -> str:
    """Convert uuid/id to guid
    """
    namespace = uuid
    guid = str(uuid3(namespace, str(id)))
    return guid
