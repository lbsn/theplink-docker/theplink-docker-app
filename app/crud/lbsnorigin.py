from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.db_models.lbsn import Origin
from app.lbsnmodels.origin import OriginCreate, OriginUpdate


def get(
        db_session: Session, *, origin_id: int) -> Optional[Origin]:
    return db_session.query(Origin).filter(
        Origin.origin_id == origin_id).first()


def get_by_name(db_session: Session, *, name: str) -> Optional[Origin]:
    return db_session.query(Origin).filter(Origin.name == name).first()


def get_multi(
        db_session: Session, *, skip=0, limit=100) -> List[Optional[Origin]]:
    return db_session.query(Origin).offset(skip).limit(limit).all()


def create(db_session: Session, *, origin_in: OriginCreate) -> Origin:
    origin_in_data = jsonable_encoder(origin_in)
    origin = Origin(**origin_in_data)
    db_session.add(origin)
    db_session.commit()
    db_session.refresh(origin)
    return origin


def update(
        db_session: Session, *,
        origin: Origin, origin_in: OriginUpdate) -> Origin:
    origin_data = jsonable_encoder(origin)
    update_data = origin_in.dict(skip_defaults=True)
    for field in origin_data:
        if field in update_data:
            setattr(origin, field, update_data[field])
    db_session.add(origin)
    db_session.commit()
    db_session.refresh(origin)
    return origin
