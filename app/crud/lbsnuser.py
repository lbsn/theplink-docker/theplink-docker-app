from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.db_models.lbsn import User as DBUser
from app.lbsnmodels.user import (
    UserCreate, UserUpdate, User,
    user_userindb_mapping, userindb_db_mapping,
    db_userindb_mapping, db_user_mapping)
from app.crud.utils import check_default_origin


def get(
        db_session: Session, *,
        user_guid: str, origin_id: int = None) -> Optional[DBUser]:
    origin_id = check_default_origin(origin_id)
    return db_session.query(DBUser).filter(
        DBUser.origin_id == origin_id,
        DBUser.user_guid == user_guid).first()


def get_by_name(
        db_session: Session, *,
        user_name: str, origin_id: int = None) -> Optional[DBUser]:
    origin_id = check_default_origin(origin_id)
    return db_session.query(DBUser).filter(
        DBUser.origin_id == origin_id,
        DBUser.user_name == user_name).first()


def get_multi(
        db_session: Session, *, skip=0, limit=100) -> List[Optional[DBUser]]:
    return db_session.query(DBUser).offset(skip).limit(limit).all()


def create(
        db_session: Session, *,
        user_in: UserCreate) -> DBUser:
    # map to userindb
    user_in = user_userindb_mapping(user_in)
    # map to db structure
    user = userindb_db_mapping(user_in)
    # only after every validation is passing,
    # submit and commit to db
    db_session.add(user)
    db_session.commit()
    db_session.refresh(user)
    return user


def update(
        db_session: Session, *,
        user: DBUser, user_in: UserUpdate) -> DBUser:
    """Updates user in db based on user_in

    Note names: variables are named according to their
    values, e.g. user_db means 'this is the userdata
    of the user in the db', user_in means: 'this is the
    data retrieved for the user to be updated'.
    Function names, on the other hand, are named according
    to their type mapping. E.g. db_userindb_mapping
    maps the 'DBUser' class (SQLAlchemy) to the class
    'UserInDb' (pydantic model), etc. Therefore:
    'user_db = db_user_mapping(user)' means:
    take the user data from the db (DBUser class)
    and map it into a variable user_db of (User class)
    """

    user_db = db_userindb_mapping(user)
    user_in = user_userindb_mapping(user_in)

    user_data = jsonable_encoder(user_db)
    update_data = user_in.dict(skip_defaults=True)
    for field in user_data:
        if field in update_data:
            # skip fields that cannot be updated
            if field == "user_guid" or field == "origin_id":
                continue
            setattr(user, field, update_data[field])
    db_session.add(user)
    db_session.commit()
    db_session.refresh(user)
    return user
