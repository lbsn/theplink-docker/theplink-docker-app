from typing import List, Optional, Tuple

from fastapi.encoders import jsonable_encoder
from geoalchemy2.elements import WKBElement
from sqlalchemy.orm import Session

from app.crud.utils import check_default_origin
from app.db_models.lbsn import Post as DBPost
from app.lbsnmodels.post import (Post, PostCreate, PostUpdate, db_post_mapping,
                                 db_postindb_mapping, post_postindb_mapping,
                                 postindb_db_mapping)


def get(
        db_session: Session, *,
        origin_id: int = None,
        post_guid: int = None) -> Optional[DBPost]:
    origin_id = check_default_origin(origin_id)
    return db_session.query(DBPost).filter(
        DBPost.origin_id == origin_id,
        DBPost.post_guid == post_guid).first()


def get_multi(
        db_session: Session, *, skip=0, limit=100) -> List[Optional[DBPost]]:
    return db_session.query(DBPost).offset(skip).limit(limit).all()


def get_multi_map(
        db_session: Session, *, skip=0,
        limit=1000, origin_id: int = None) -> List[
            Optional[Tuple[WKBElement, str]]]:
    if origin_id is None:
        origin_id = 0
    return (
        db_session.query(DBPost)
        .filter(DBPost.origin_id == origin_id)
        .offset(skip)
        .limit(limit)
        .with_entities(DBPost.post_latlng, DBPost.post_guid)
        .all()
    )


def get_multi_by_owner(
    db_session: Session, *,
    user_guid: int,
    origin_id: int = 0,
    skip=0, limit=100
) -> List[Optional[DBPost]]:
    return (
        db_session.query(DBPost)
        .filter(
            DBPost.origin_id == origin_id,
            DBPost.user_guid == user_guid)
        .offset(skip)
        .limit(limit)
        .all()
    )


def create(
        db_session: Session, *,
        post_in: PostCreate,
        user_guid: str = None,
        post_guid: str = None) -> DBPost:
    # map to postindb
    post_in = post_postindb_mapping(
        post_in, user_guid=user_guid, post_guid=post_guid)
    # map to db structure
    post = postindb_db_mapping(post_in)
    # only after every validation is passing,
    # submit and commit to db
    db_session.add(post)
    db_session.commit()
    db_session.refresh(post)
    return post


def update(
        db_session: Session, *,
        post: DBPost, post_in: PostUpdate) -> DBPost:
    """Updates post in db based on post_in
    """
    post_db = db_postindb_mapping(post)
    post_in = post_postindb_mapping(post_in, post_guid=post.post_guid)

    post_data = jsonable_encoder(post_db)
    update_data = post_in.dict(skip_defaults=True)
    for field in post_data:
        if field in update_data:
            setattr(post, field, update_data[field])

    db_session.add(post)
    db_session.commit()
    db_session.refresh(post)
    return post
