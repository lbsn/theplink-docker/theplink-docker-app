from geoalchemy2.types import Geometry
from sqlalchemy import (BigInteger, Boolean, CheckConstraint, Column,
                        DateTime, ForeignKey, ForeignKeyConstraint, Index,
                        Integer, Text)
from sqlalchemy.dialects.postgresql.hstore import HSTORE
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import relationship
from sqlalchemy.schema import FetchedValue
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy import text

from app.db.base_class import LBSNBase
from app.db_models.utils import NULL_ISLAND_HEX


class Origin(LBSNBase):
    __tablename__ = 'origin'

    origin_id = Column(
        Integer, primary_key=True, server_default=FetchedValue())
    name = Column(Text, unique=True)


class Language(LBSNBase):
    __tablename__ = 'language'

    language_short = Column(Text, primary_key=True)
    language_name = Column(Text)
    language_name_de = Column(Text)


class Country(LBSNBase):
    __tablename__ = 'country'

    country_guid = Column(Text, primary_key=True, nullable=False)
    origin_id = Column(ForeignKey('origin.origin_id'),
                       primary_key=True, nullable=False)
    name = Column(Text)
    geom_center = Column(Geometry('POINT', 4326), index=True)
    geom_area = Column(Geometry('POLYGON', 4326))
    url = Column(Text)
    name_alternatives = Column(
        ARRAY(Text), nullable=False, server_default=FetchedValue())

    origin = relationship(
        'Origin',
        primaryjoin='Country.origin_id == Origin.origin_id',
        backref='countries',
        viewonly=True)


class City(LBSNBase):
    __tablename__ = 'city'
    __table_args__ = (
        ForeignKeyConstraint(
            ['origin_id', 'country_guid'],
            ['country.origin_id', 'country.country_guid']),
    )

    city_guid = Column(Text, primary_key=True, nullable=False)
    origin_id = Column(
        ForeignKey('origin.origin_id'), primary_key=True, nullable=False)
    name = Column(Text)
    geom_center = Column(Geometry('POINT', 4326), index=True)
    geom_area = Column(Geometry('POLYGON', 4326))
    country_guid = Column(Text)
    url = Column(Text)
    name_alternatives = Column(
        ARRAY(Text), nullable=False, server_default=FetchedValue())
    sub_type = Column(Text)

    origin = relationship(
        'Country',
        primaryjoin=(
            'and_(City.origin_id == Country.origin_id, '
            'City.country_guid == Country.country_guid)'),
        backref='cities',
        viewonly=True)
    origin1 = relationship(
        'Origin',
        primaryjoin='City.origin_id == Origin.origin_id',
        backref='cities',
        viewonly=True)


class Place(LBSNBase):
    __tablename__ = 'place'
    __table_args__ = (
        ForeignKeyConstraint(
            ['origin_id', 'city_guid'], ['city.origin_id', 'city.city_guid']),
        Index('idx_placecity', 'origin_id', 'city_guid'),
    )

    origin_id = Column(ForeignKey('origin.origin_id'),
                       primary_key=True, nullable=False)
    place_guid = Column(Text, primary_key=True, nullable=False)
    name = Column(Text)
    post_count = Column(BigInteger)
    url = Column(Text)
    geom_center = Column(
        Geometry('POINT', 4326),
        default=NULL_ISLAND_HEX,
        index=True)
    geom_area = Column(Geometry('POLYGON', 4326))
    city_guid = Column(Text)
    name_alternatives = Column(
        ARRAY(Text), nullable=False, server_default=FetchedValue())
    place_description = Column(Text)
    place_website = Column(Text)
    place_phone = Column(Text)
    address = Column(Text)
    zip_code = Column(Text)
    #attributes = Column(HSTORE(Text))
    attributes = Column(MutableDict.as_mutable(HSTORE))
    checkin_count = Column(BigInteger)
    like_count = Column(BigInteger)
    modified = Column(DateTime, index=True, server_default=FetchedValue())

    origin = relationship(
        'City', primaryjoin=(
            'and_(Place.origin_id == City.origin_id, '
            'Place.city_guid == City.city_guid)'),
        backref='places',
        viewonly=True)
    origin1 = relationship(
        'Origin',
        primaryjoin='Place.origin_id == Origin.origin_id',
        backref='places',
        viewonly=True)


class User(LBSNBase):
    __tablename__ = 'user'

    origin_id = Column(
        ForeignKey('origin.origin_id'), primary_key=True, nullable=False)
    user_guid = Column(Text, primary_key=True, nullable=False)
    user_name = Column(Text)
    user_fullname = Column(Text)
    follows = Column(BigInteger)
    followed = Column(BigInteger)
    biography = Column(Text)
    post_count = Column(BigInteger)
    url = Column(Text)
    is_private = Column(Boolean)
    is_available = Column(Boolean)
    user_language = Column(ForeignKey('language.language_short'))
    user_location = Column(Text)
    user_location_geom = Column(Geometry('POINT', 4326))
    liked_count = Column(BigInteger)
    active_since = Column(DateTime)
    profile_image_url = Column(Text)
    user_timezone = Column(Text)
    user_utc_offset = Column(Integer)
    user_groups_member = Column(ARRAY(Text))
    user_groups_follows = Column(ARRAY(Text))
    group_count = Column(BigInteger)
    modified = Column(DateTime, index=True, server_default=FetchedValue())

    origin = relationship(
        'Origin',
        primaryjoin='User.origin_id == Origin.origin_id',
        backref='users',
        viewonly=True)
    language = relationship(
        'Language',
        primaryjoin='User.user_language == Language.language_short',
        backref='users',
        viewonly=True)


class UserGroup(LBSNBase):
    __tablename__ = 'user_groups'
    __table_args__ = (
        ForeignKeyConstraint(
            ['origin_id', 'user_owner'],
            ['user.origin_id', 'user.user_guid']),
    )

    origin_id = Column(
        ForeignKey('origin.origin_id'), primary_key=True, nullable=False)
    usergroup_guid = Column(Text, primary_key=True, nullable=False)
    usergroup_name = Column(Text)
    usergroup_description = Column(Text)
    member_count = Column(BigInteger)
    usergroup_createdate = Column(DateTime)
    user_owner = Column(Text)

    origin = relationship(
        'User',
        primaryjoin=(
            'and_(UserGroup.origin_id == User.origin_id, UserGroup.user_owner '
            '== User.user_guid)'), backref='user_groups', viewonly=True)
    origin1 = relationship(
        'Origin',
        primaryjoin='UserGroup.origin_id == Origin.origin_id',
        backref='user_groups', viewonly=True)


class Post(LBSNBase):
    __tablename__ = 'post'
    __table_args__ = (
        CheckConstraint(
            "post_geoaccuracy = ANY (ARRAY['unknown'::text, 'latlng'::text,"
            " 'place'::text, 'city'::text, 'country'::text])"),
        ForeignKeyConstraint(
            ['origin_id', 'city_guid'],
            ['city.origin_id', 'city.city_guid']),
        ForeignKeyConstraint(
            ['origin_id', 'country_guid'],
            ['country.origin_id', 'country.country_guid']),
        ForeignKeyConstraint(
            ['origin_id', 'place_guid'],
            ['place.origin_id', 'place.place_guid']),
        ForeignKeyConstraint(
            ['origin_id', 'user_guid'],
            ['user.origin_id', 'user.user_guid']),
        Index('idx_postcity', 'origin_id', 'city_guid'),
        Index('idx_postcountry', 'origin_id', 'country_guid'),
        Index('idx_postplace', 'origin_id', 'place_guid'),
        Index('idx_postuser', 'origin_id', 'user_guid'),
    )

    origin_id = Column(
        ForeignKey('origin.origin_id'),
        primary_key=True, nullable=False)
    post_guid = Column(Text, primary_key=True, nullable=False)
    post_latlng = Column(
        Geometry('POINT', 4326), nullable=False,
        default=NULL_ISLAND_HEX,
        index=True)
    place_guid = Column(Text)
    city_guid = Column(Text)
    country_guid = Column(Text)
    user_guid = Column(Text)
    post_publish_date = Column(DateTime, index=True)
    post_body = Column(Text)
    post_geoaccuracy = Column(Text, default=text("'unknown'"))
    hashtags = Column(ARRAY(Text))
    emoji = Column(ARRAY(Text))
    post_like_count = Column(BigInteger)
    post_comment_count = Column(BigInteger)
    post_views_count = Column(BigInteger)
    post_title = Column(Text)
    post_create_date = Column(DateTime, index=True)
    post_thumbnail_url = Column(Text)
    post_url = Column(Text)
    post_type = Column(Text, default=text("'text'"))
    post_filter = Column(Text)
    post_quote_count = Column(BigInteger)
    post_share_count = Column(BigInteger)
    post_language = Column(ForeignKey('language.language_short'))
    input_source = Column(Text)
    user_mentions = Column(ARRAY(Text))
    modified = Column(DateTime, index=True, server_default=FetchedValue())
    post_content_license = Column(Integer)

    origin = relationship(
        'City', primaryjoin=(
            'and_(Post.origin_id == City.origin_id, Post.city_guid '
            '== City.city_guid)'), backref='posts', viewonly=True)
    origin1 = relationship(
        'Country', primaryjoin=(
            'and_(Post.origin_id == Country.origin_id, Post.country_guid '
            '== Country.country_guid)'), backref='posts', viewonly=True)
    origin2 = relationship(
        'Place', primaryjoin=(
            'and_(Post.origin_id == Place.origin_id, Post.place_guid == '
            'Place.place_guid)'), backref='posts', viewonly=True)
    origin3 = relationship(
        'User', primaryjoin=(
            'and_(Post.origin_id == User.origin_id, Post.user_guid == '
            'User.user_guid)'), backref='posts', viewonly=True)
    origin4 = relationship(
        'Origin',
        primaryjoin='Post.origin_id == Origin.origin_id',
        backref='posts', viewonly=True)
    language = relationship(
        'Language',
        primaryjoin='Post.post_language == Language.language_short',
        backref='posts', viewonly=True)


class PostReaction(LBSNBase):
    __tablename__ = 'post_reaction'
    __table_args__ = (
        CheckConstraint(
            "reaction_type = ANY (ARRAY['unknown'::text, 'share'::text, "
            "'comment'::text, 'quote'::text, 'like'::text, 'emoji'::text, "
            "'other'::text])"),
        ForeignKeyConstraint(
            ['origin_id', 'referencedpost_guid'],
            ['post.origin_id', 'post.post_guid']),
        ForeignKeyConstraint(
            ['origin_id', 'referencedpostreaction_guid'],
            ['post_reaction.origin_id', 'post_reaction.reaction_guid']),
        ForeignKeyConstraint(
            ['origin_id', 'user_guid'],
            ['user.origin_id', 'user.user_guid']),
    )

    origin_id = Column(
        ForeignKey('origin.origin_id'), primary_key=True, nullable=False)
    reaction_guid = Column(Text, primary_key=True, nullable=False)
    referencedpost_guid = Column(Text)
    user_guid = Column(Text)
    reaction_latlng = Column(
        Geometry('POINT', 4326),
        default=NULL_ISLAND_HEX
    )
    reaction_type = Column(Text)
    reaction_date = Column(DateTime)
    reaction_content = Column(Text)
    reaction_like_count = Column(BigInteger)
    user_mentions = Column(ARRAY(Text))
    referencedpostreaction_guid = Column(Text)
    modified = Column(DateTime, server_default=FetchedValue())

    origin = relationship(
        'Post', primaryjoin=(
            'and_(PostReaction.origin_id == Post.origin_id, '
            'PostReaction.referencedpost_guid == Post.post_guid)'),
        backref='post_reactions', viewonly=True)
    origin1 = relationship(
        'PostReaction',
        remote_side=[origin_id, reaction_guid],
        primaryjoin=(
            'and_(PostReaction.origin_id == PostReaction.origin_id, '
            'PostReaction.referencedpostreaction_guid == '
            'PostReaction.reaction_guid)'),
        backref='post_reactions',
        viewonly=True)
    origin2 = relationship(
        'User', primaryjoin=(
            'and_(PostReaction.origin_id == User.origin_id, '
            'PostReaction.user_guid == User.user_guid)'),
        backref='post_reactions',
        viewonly=True)
    origin3 = relationship(
        'Origin',
        primaryjoin='PostReaction.origin_id == Origin.origin_id',
        backref='post_reactions',
        viewonly=True)


class Term(LBSNBase):
    __tablename__ = 'term'
    __table_args__ = (
        Index('idx_term', 'origin_id', 'term'),
    )

    origin_id = Column(
        ForeignKey('origin.origin_id'), primary_key=True, nullable=False)
    term = Column(Text, primary_key=True, nullable=False)
    term_post_count = Column(BigInteger)

    origin = relationship(
        'Origin',
        primaryjoin='Term.origin_id == Origin.origin_id',
        backref='terms',
        viewonly=True)
