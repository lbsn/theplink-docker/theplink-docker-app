
from geoalchemy2.elements import WKTElement
# define Null Island (Point 0 0) with srid=4326
# as PostGis hex-encoded (E)WKB for speed
NULL_ISLAND_HEX = '0101000020E610000000000000000000000000000000000000'
# for internal api handling of null island
# NULL_ISLAND_WKT = f'SRID=4326;POINT(0 0)'
NULL_ISLAND_WKT = WKTElement(
    f"Point (0 0)",
    srid=4326, extended=True)
