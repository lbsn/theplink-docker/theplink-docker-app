"""Add lbsn structure triggers

Revision ID: aae93615f28a
Revises: 839d0aff8f0b
Create Date: 2019-06-07 12:42:10.184791

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'aae93615f28a'
down_revision = '839d0aff8f0b'
branch_labels = None
depends_on = None


def upgrade():
    # get full connection
    connection = op.get_bind()

    # Triggers for updating the [modified]-column,
    # indicating the timestamps of the insertion or latest update for rows
    connection.execute(
        """
        DROP TRIGGER IF EXISTS update_user_modtime on data.user;
        CREATE TRIGGER update_user_modtime BEFORE UPDATE ON data.user FOR EACH ROW EXECUTE PROCEDURE extensions.update_modified_column();
        DROP TRIGGER IF EXISTS update_post_modtime on data.post;
        CREATE TRIGGER update_post_modtime BEFORE UPDATE ON data.post FOR EACH ROW EXECUTE PROCEDURE extensions.update_modified_column();
        DROP TRIGGER IF EXISTS update_place_modtime on data.place;
        CREATE TRIGGER update_place_modtime BEFORE UPDATE ON data.place FOR EACH ROW EXECUTE PROCEDURE extensions.update_modified_column();
        DROP TRIGGER IF EXISTS update_reaction_modtime on data.post_reaction;
        CREATE TRIGGER update_reaction_modtime BEFORE UPDATE ON data.post_reaction FOR EACH ROW EXECUTE PROCEDURE extensions.update_modified_column();
        """
    )
    trigger_mod_comment = (
        'This trigger will always update the modified column, '
        'no matter if data actually has changed or not')
    connection.execute(
        f"""
        COMMENT ON TRIGGER update_user_modtime on data.user
                IS '{trigger_mod_comment}';
        """)
    connection.execute(
        f"""
        COMMENT ON TRIGGER update_post_modtime on data.post
                IS '{trigger_mod_comment}';
        """)
    connection.execute(
        f"""
        COMMENT ON TRIGGER update_place_modtime on data.place
                IS '{trigger_mod_comment}';
        """)
    connection.execute(
        f"""
        COMMENT ON TRIGGER update_reaction_modtime on data.post_reaction
                IS '{trigger_mod_comment}';
        """)

    # trig_UpdatePlaceGeom
    connection.execute(
        """
        DROP TRIGGER IF EXISTS trig_UpdatePlaceGeom on data.place;
        CREATE TRIGGER trig_UpdatePlaceGeom
        AFTER UPDATE
        ON data.place
        FOR EACH ROW
        WHEN (pg_trigger_depth() < 2)
        EXECUTE PROCEDURE extensions.func_UpdatePlaceGeom();
        """
    )
    connection.execute(
        """
        COMMENT ON TRIGGER update_reaction_modtime on data.post_reaction
                IS 'Triggers extensions.func_UpdatePlaceGeom() after update. 
                In case exact spatial information is missing, 
                this will substite lower resolution spatial 
                information for concerned rows. 
                (e.g. instead of lat/lng: place or city).';
        """)

    # trig_UpdateCountryID
    connection.execute(
        """
        DROP TRIGGER IF EXISTS trig_UpdateCountryID on data.city;
        CREATE TRIGGER trig_UpdateCountryID
        AFTER UPDATE
        ON data.city
        FOR EACH ROW
        WHEN (pg_trigger_depth() < 2)
        EXECUTE PROCEDURE extensions.func_UpdateCountryID();
        """
    )
    connection.execute(
        """
        COMMENT ON TRIGGER update_reaction_modtime on data.post_reaction
                IS 'Triggers extensions.func_UpdateCountryID() after update.
                Will add country id to concerned posts if none 
                has been added before.';
        """)

    # trig_SubstitutePostLatLng_Ins
    connection.execute(
        """
        DROP TRIGGER IF EXISTS trig_SubstitutePostLatLng_Ins on data.post;
        CREATE TRIGGER trig_SubstitutePostLatLng_Ins
        BEFORE INSERT
        ON data.post
        FOR EACH ROW
        WHEN (pg_trigger_depth() < 1)
        EXECUTE PROCEDURE extensions.func_SubstitutePostLatLng_Ins();
        """
    )
    connection.execute(
        """
        COMMENT ON TRIGGER update_reaction_modtime on data.post_reaction
                IS 'Triggers extensions.func_SubstitutePostLatLng_Ins() 
                after update. Will update concerned posts 
                (rows post.post_latlng and post.post_geoaccuracy) 
                with the hightest spatial information available. E.g. if 
                for some posts, only city-location was available 
                so far and place-location becomes available, this will use 
                the spatial location of the place instead 
                and refresh post.geoaccuracy to "place".';
        """)

    # trig_SubstitutePostLatLng_Upd
    connection.execute(
        """
        DROP TRIGGER IF EXISTS trig_SubstitutePostLatLng_Upd on data.post;
        CREATE TRIGGER trig_SubstitutePostLatLng_Upd
        BEFORE UPDATE
        ON data.post
        FOR EACH ROW
        WHEN (pg_trigger_depth() < 1)
        EXECUTE PROCEDURE extensions.func_SubstitutePostLatLng_Upt();

        """
    )
    connection.execute(
        """
        COMMENT ON TRIGGER update_reaction_modtime on data.post_reaction
                IS 'Triggers extensions.func_SubstitutePostLatLng_upt() 
                after update. This is the same function as above, 
                just that this works for updates, not for new inserts.';
        """)
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
