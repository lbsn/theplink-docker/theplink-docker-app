"""Add maintenance settings migrations

Revision ID: 143742b69f56
Revises: aae93615f28a
Create Date: 2019-06-07 13:08:22.664839

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '143742b69f56'
down_revision = 'aae93615f28a'
branch_labels = None
depends_on = None


def upgrade():
    # get full connection
    connection = op.get_bind()
    # Auto Vacuum appears too infrequently on data.post, reduce limit
    connection.execute(
        """
        ALTER TABLE data.post SET (autovacuum_vacuum_scale_factor = 0.01);
        ALTER TABLE data.post SET (autovacuum_vacuum_threshold = 10000);
        """)
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
